let params = new URLSearchParams(window.location.search)

//Store the courseId from the URL Query String in a variable
let courseId = params.get('courseId')
//console.log(courseId)

//get the token from localStorage

let token = localStorage.getItem("token")
//console.log(token)

let adminUser = localStorage.getItem('isAdmin')
//console.log(adminUser)

let courseName = document.querySelector("#courseName")
let courseDesc = document.querySelector("#courseDesc")
let coursePrice = document.querySelector("#coursePrice")
let enrollContainer = document.querySelector("#enrollContainer")
let table = document.querySelector('#enrolleesTable')


//Get details of a single course
//pass ${courseId} to endpoint api/courses/:id
fetch(`https://fierce-mountain-35664.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	courseName.innerHTML = data.name
	courseDesc.innerHTML = data.description
	coursePrice.innerHTML = data.price

	//Enroll button
	if (!adminUser || adminUser === "false") {

		
		enrollContainer.innerHTML = `

		<div class="enrollBox">
		<button id="enrollButton"
		class="btn btn-block btn-warning">Learn Skill</button>
		</div>`



			document.querySelector("#enrollButton").addEventListener("click", ()=>{
			//console.log("im working")

			//add fetch to enroll our user:
			fetch('https://fierce-mountain-35664.herokuapp.com/api/users/enroll', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				},
				body: JSON.stringify({
					courseId: courseId
				})
			})
			.then(res => res.json())
			.then(data => {
				if (data === true) {
				//show an alert to the course page after enrolling.
				alert('You obtain a skill.')
				window.location.replace('./courses.html')
			} else {
				alert('Hey Human!,Register first! ')
				window.location.replace('register.html');
				}




			})
		})
	}
})



//Get courses by ID
		fetch(`https://fierce-mountain-35664.herokuapp.com/api/courses/${courseId}`)
		.then(res => res.json())
		.then(data => {
			//console.log(data)
		

			if(data.enrollees != undefined && data.enrollees.length > 0 ){
	      	//console.log(data.enrollees.length)

		      	enrollData = data.enrollees.map(users => {
		  		//console.log(users)
		  		//console.log(users.userId)

		      		//match id to course names
		      		fetch(`https://fierce-mountain-35664.herokuapp.com/api/users/${users.userId}`)
		      		.then(res => res.json())
		     		.then(userdata => {
		     		 	//console.log(userdata)


		     		 	if(adminUser === "true") {
		     		 		let students = data.enrollees
							console.log(students)

							enrollContainer.innerHTML = 
								`
							<div class="col-md-12">
								<div class="jumbotron text-center my-5">
								<div id="enrolledContainer">
									<h3>Enrollees:</h3>
								</div>
								</div>
							</div>
								`
								let userEnrollees = document.querySelector("#enrolledContainer")
								data.enrollees.forEach(enrollees => {

									fetch (`https://fierce-mountain-35664.herokuapp.com/api/users/${enrollees.userId}`,	 {
									headers: {
										"Content-Type": "application/json",
											}
								})
								.then(res => res.json())
									.then(userdata => {
										return userEnrollees.innerHTML +=
										`
											<div class="card">
												<div class="card-body">
											   		<h5 class="card-title">${userdata.lastName}, ${userdata.firstName}</h5>
											   			<p>Email Address: ${userdata.email}</p>
														<p> Mobile Number: ${userdata.mobileNo} </p>
											  			 <h6 class="card-subtitle mb-2 text-muted">Enrolled On: ${new Date(enrollees.enrolledOn).toLocaleString()}</h6>
											  			 <h6 class="card-subtitle mb-2 text-muted">Status:${enrollees.status}</h6>
												</div>
											</div>

											`
								})

										})
														
								
						 } else{

						 }
					})

				 })
		      }
		  })