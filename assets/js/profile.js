let profileContainer = document.querySelector("#profileContainer")
let token = localStorage.getItem("token")
let adminUser = localStorage.getItem("isAdmin")


//fetch loggedin user details

fetch (`https://fierce-mountain-35664.herokuapp.com/api/users/details`, {
	headers: {
		"Content-Type": "application/json",
		"Authorization": `Bearer ${token}`
				}
	})
	.then(res => res.json())
	.then(data => {

		if (adminUser === "true") {

	//show profile container Admin Logged-In

	profileContainer.innerHTML =
		`
		<div class="col-md-12">
			<div class="jumbotron text-center my-5" id="profileBg">
				<h2 class="my-2">${data.lastName}, ${data.firstName}</h2>
					<p>Email Address: ${data.email}</p>
					<p> Mobile Number: ${data.mobileNo} </p>
					<div id="enrolledUsers">
				<h3>You have active humans!</h3>
			</div>
			</div>
		</div>
		
		`
let activeUsers = document.querySelector("#enrolledUsers")


fetch ('https://fierce-mountain-35664.herokuapp.com/api/users/enrolled')
.then(res => res.json())
.then(data => {

data.forEach(user => {


	activeUsers.innerHTML +=
			`
			<div class="card" id="profileCourseBg">
				<div class="card-body">
			   		<h5 class="card-title">${user.lastName}, ${user.firstName}</h5>
							<p>Email Address: ${user.email}</p>
							
				</div>
			</div>

			`

   })


	console.log(data)
})




//To fix unexpected end of JSON Input later on in console..

	// .then(res => res.json())
	// .then(data => {

	// 	console.log(users)
	// return activeUsers.innerHTML +=
	// 		`
	// 		<div class="card" id="profileCourseBg">
	// 			<div class="card-body">
	// 		   		<h5 class="card-title">${data.lastName}, ${data.firstName}</h5>
	// 						<p>Email Address: ${data.email}</p>
							
	// 			</div>
	// 		</div>

	// 		`
	// 		})

		


	} else if (adminUser === "false") {



//let coursesEnrolled = []

	profileContainer.innerHTML =
		`
		<div class="col-md-12">
			<div class="jumbotron text-center my-5" id="profileBg">
				<h1 class="my-2">${data.lastName}, ${data.firstName}</h1>
					<p>Email Address: ${data.email}</p>
					<p> Mobile Number: ${data.mobileNo} </p>
			<div id="enrollmentsContainer">
				<h3>Chosen Skills:</h3>
			</div>
			</div>
		</div>

		`

let courseEnrollments = document.querySelector("#enrollmentsContainer")

data.enrollments.forEach(enrollments => {


fetch (`https://fierce-mountain-35664.herokuapp.com/api/courses/${enrollments.courseId}`,	 {
	headers: {
		"Content-Type": "application/json",
			}
})


//To fix unexpected end of JSON Input later on in console..

	.then(res => res.json())
	.then(data => {
	return courseEnrollments.innerHTML +=
			`
			<div class="card" id="profileCourseBg">
				<div class="card-body">
			   		<h5 class="card-title">${data.name}</h5>
			  			 <h6 class="card-subtitle mb-2 ">Enrolled On: ${new Date(enrollments.enrolledOn).toLocaleString()}</h6>
			  			 <h6 class="card-subtitle mb-2 ">Status:${enrollments.status}</h6>
				</div>
			</div>

			`
			})

		})
	}

})



