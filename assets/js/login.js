let loginForm = document.querySelector("#logInUser");

loginForm.addEventListener("submit", (e)=>{

	e.preventDefault();

	let email = document.querySelector("#userEmail").value
	let password = document.querySelector("#password").value

	console.log(email);
	console.log(password);

	if(email=="" || password == ""){
		alert("Please enter your email and/or password.")

	}else {
		fetch('https://fierce-mountain-35664.herokuapp.com/api/users/login', {
			method: "POST",
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				email: email,
				password: password
			})

		})

		.then(res =>res.json())
		.then(data=>{

				if(data.accessToken){

					localStorage.setItem("token", data.accessToken)
					fetch('https://fierce-mountain-35664.herokuapp.com/api/users/details', {
						headers: {
							Authorization: `Bearer ${data.accessToken}`

						}
					
					})
					.then(res =>res.json())
					.then(data =>{
						localStorage.setItem("Id",data._id)
						localStorage.setItem("isAdmin",data.isAdmin)


						window.location.replace("./enter.html")
					})

				}else{
					alert("Login Failed. Something went wrong.")
				}
		})
	}
	

})