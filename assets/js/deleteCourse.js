let params = new URLSearchParams(window.location.search)

// console.log(params.has('courseId'));
let courseId = params.get('courseId')
// console.log(courseId);
let token = localStorage.getItem("token");
// console.log(token);

fetch(`https://fierce-mountain-35664.herokuapp.com/api/courses/${courseId}`,{

	method: 'Delete',
	headers: {
		'Content-Type' : 'application/json',
		'Authorization' : `Bearer ${token}`
	},
	body: JSON.stringify({
		courseId: courseId
	})
})

.then(res => res.json())
.then(data =>{
	if (data){
	window.location.replace('./courses.html')
}else{
	alert("Something went wrong.")
}
}) 

