let formSubmit = document.querySelector("#createCourse")

//add event listener:

formSubmit.addEventListener("submit", (e) =>{
	e.preventDefault()


	let courseName = document.querySelector("#courseName").value
	let description = document.querySelector("#courseDescription").value
	let price = document.querySelector("#coursePrice").value

	
	let token = localStorage.getItem("token")
	console.log(token)


	fetch('https://fierce-mountain-35664.herokuapp.com/api/courses', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName,
			description: description,
			price: price
		})
	})
	.then(res => res.json())
	.then(data =>{


		if(data === true){
			window.location.replace('./courses.html')
		}else {
			alert("Course Creation Failed. Something Went Wrong")
		}
	})

})



