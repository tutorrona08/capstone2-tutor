let adminUser = localStorage.getItem("isAdmin");
console.log(adminUser);
let token = localStorage.getItem('token');


let cardFooter; 


fetch('https://fierce-mountain-35664.herokuapp.com/api/courses', {
 method: 'GET',
 headers: {
 	'Authorization': `Bearer ${token}`
 }
})
.then(res => res.json())
.then(data => {
console.log(data);
console.log(token);
// if(token == null){
//   alert('Login first')
//   window.location.replace('login.html');
  
// }
    
	let courseData;

		if(data.length < 1) {
		courseData = "No courses available"
		} else{

		courseData = data.map(course => { //map function returns an array.

			console.log(course._id);


			if(adminUser === "false" || !adminUser) { 
				cardFooter =
				` 
                
                 <a href="./course.html?courseId=${course._id}" value=${course._id} class="btn btn-warning text-white btn-block editButton"> Activate Skill </a>
                 

				`
              
			} else { 
                cardFooter = 
                `
                
                <a href="./course.html?courseId=${course._id}" value=${course._id}  id="viewButton" class="btn btn-dark text-white btn-block secondaryButton">View Humans </a>
                <a href="./editCourse.html?courseId=${course._id}" value=${course._id} id="editButton" class="btn btn-primary text-white btn-block editButton"> Edit Skill </a>
                `

                let isActive = course.isActive;
                if(isActive){
                    cardFooter = cardFooter +
                    ` <button id= '${course._id}' value= 1 onclick="enableDisable('${course._id}',this.value)"  id= "enableButton" class="btn btn-danger text-white btn-block dangerButton"> Active Skill </button> 
                    `

                } else {
                    cardFooter = cardFooter + 
                    `
                    <button id= '${course._id}' value= 0 onclick="enableDisable('${course._id}',this.value)" id="disableButton" class="btn btn-danger text-white btn-block warningButton"> Deactivate Skill </button> 
                    `
                }
            }
            	
             
		

			return (
			
                   `

                    <div class="col-md-6 my-3">
		                <div class='card'id="coursesCardBackground">
		                    <div class='card-body' >
		                        <h5 class='card-title'>${course.name}</h5>
		                            <p class='card-text text-left'>
		                                    ${course.description}
		                            </p>
		                            <p class='card-text text-right'>
		                                   ₱ ${course.price}
		                            </p>

		                            </div>
		                            <div class='card-footer'>
		                                ${cardFooter}
		                            </div>
		                        </div>
		                    </div>

                   `



				)

		}).join("") 

		let coursesContainer = document.querySelector("#coursesContainer");

		coursesContainer.innerHTML = courseData;

	}
})


let modalButton = document.querySelector('#adminButton');

	if(adminUser == "false" || !adminUser){
	modalButton.innerHTML = null;
	} else {
		modalButton.innerHTML =
			`
   			<div class="col-md-2 offset-md-10 col-sm-4 ">
			<a href="./addCourse.html" id="addCoursebtn" class="btn btn-block btn-danger">Add Skill </a>
      <a href="./profile.html" id="addCoursebtn" class="btn btn-block btn-warning">Domain </a>
			</div>



			`

}

function enableDisable(courseId, isActive){
	console.log(isActive);
    let button= document.getElementById(courseId);
    if(isActive== 1){
      
        fetch(`https://fierce-mountain-35664.herokuapp.com/api/courses/${courseId}`,{
            method: 'PUT',
                   headers: {
                   'Authorization': `Bearer ${token}`
                  }
        	
          })
        .then(res => res.json())
        .then(data => {
        	console.log(data);
      
        })

        button.innerHTML = `Deactivate Skill`;
        button.innerText= `Deactivate Skill`;
        button.value = 0;
    } 

    else { 
    	fetch(`https://fierce-mountain-35664.herokuapp.com/api/courses/${courseId}`,{
            method: 'DELETE',
                   headers: {
                   'Authorization': `Bearer ${token}`
                  }
        
          })
        .then(res => res.json())
        .then(data => {
        	console.log(data);

        })



        button.innerHTML = `Reactivate Skill`;
        button.innerText = `Reactivate Skill`;

        button.value = 1;
        alert('Successfully disabled');
    }



}