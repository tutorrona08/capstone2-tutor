let userId = localStorage.getItem("Id")

console.log(userId);

let firstName = document.querySelector("#profileFirstName")
let lastName = document.querySelector("#profileLastName")
let mobileNumber = document.querySelector("#profileMobileNumber")






fetch(`https://fierce-mountain-35664.herokuapp.com/api/users/${userId}`)
.then(res => res.json())
.then(data => {

	console.log(data)


	firstName.placeholder = data.firstName
	lastName.placeholder = data.lastName
	mobileNumber.placeholder = data.mobileNo
  
	
	
	
	firstName.value = data.firstName
	lastName.value = data.lastName
	mobileNumber.value = data.mobileNo
 
	
	
	
})

document.querySelector("#editProfile").addEventListener("submit", (e) => {

	e.preventDefault()

	let profileFirstName = firstName.value
	let profileLastName = lastName.value
	let profileMobileNumber = mobileNumber.value
   
	

	let token = localStorage.getItem('token')

	
	fetch('https://fierce-mountain-35664.herokuapp.com/api/users', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            id: userId,
            firstName: profileFirstName,
            lastName: profileLastName,
            mobileNumber: profileMobileNumber,
        
            
        })
    })
    .then(res => res.json())
    .then(data => {
        
    	console.log(data)

    	
    	if(data === true){
    	   
           alert("Successful")
    	    window.location.replace("./profile.html")
    	}else{
    	   
    	    alert("something went wrong")
    	}


    })
	

})










